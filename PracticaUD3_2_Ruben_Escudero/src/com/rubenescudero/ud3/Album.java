package com.rubenescudero.ud3;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Album {
    private int id;
    private String nombreAlbum;
    private Date fechaLanzamiento;
    private List<Cancion> canciones;
    private List<Artista> artistas;

    public Album(String nombreAlbum, Date fechaLanzamiento) {
        this.nombreAlbum = nombreAlbum;
        this.fechaLanzamiento = fechaLanzamiento;
    }

    public Album() {
    }


    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", nombreAlbum='" + nombreAlbum + '\'' +
                ", fechaLanzamiento=" + fechaLanzamiento;
    }



    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_album")
    public String getNombreAlbum() {
        return nombreAlbum;
    }

    public void setNombreAlbum(String nombreAlbum) {
        this.nombreAlbum = nombreAlbum;
    }

    @Basic
    @Column(name = "fecha_lanzamiento")
    public Date getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    public void setFechaLanzamiento(Date fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return id == album.id &&
                Objects.equals(nombreAlbum, album.nombreAlbum) &&
                Objects.equals(fechaLanzamiento, album.fechaLanzamiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreAlbum, fechaLanzamiento);
    }

    @OneToMany(mappedBy = "album")
    public List<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<Cancion> canciones) {
        this.canciones = canciones;
    }

    @ManyToMany(mappedBy = "albumes")
    public List<Artista> getArtistas() {
        return artistas;
    }

    public void setArtistas(List<Artista> artistas) {
        this.artistas = artistas;
    }
}
