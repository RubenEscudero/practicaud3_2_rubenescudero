package com.rubenescudero.ud3;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Artista {
    private int id;
    private String nombreArtista;
    private Date fechaNacimiento;
    private List<Instrumento> instrumentos;
    private List<Album> albumes;

    public Artista(String nombreArtista, Date fechaNacimiento) {
        this.nombreArtista = nombreArtista;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Artista() {

    }

    @Override
    public String toString() {
        return "Artista{" +
                "id=" + id +
                ", nombreArtista='" + nombreArtista + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                '}';
    }



    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_artista")
    public String getNombreArtista() {
        return nombreArtista;
    }

    public void setNombreArtista(String nombreArtista) {
        this.nombreArtista = nombreArtista;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artista artista = (Artista) o;
        return id == artista.id &&
                Objects.equals(nombreArtista, artista.nombreArtista) &&
                Objects.equals(fechaNacimiento, artista.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreArtista, fechaNacimiento);
    }

    @OneToMany(mappedBy = "artista")
    public List<Instrumento> getInstrumentos() {
        return instrumentos;
    }

    public void setInstrumentos(List<Instrumento> instrumentos) {
        this.instrumentos = instrumentos;
    }

    @ManyToMany
    @JoinTable(name = "album_artista", catalog = "", schema = "plataforma_musica", joinColumns = @JoinColumn(name = "id_album", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_artista", referencedColumnName = "id", nullable = false))
    public List<Album> getAlbumes() {
        return albumes;
    }

    public void setAlbumes(List<Album> albumes) {
        this.albumes = albumes;
    }
}
