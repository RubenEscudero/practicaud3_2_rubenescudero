package com.rubenescudero.ud3;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Cancion {
    private int id;
    private String nombreCancion;
    private int duracion;
    private Grupo grupo;
    private Album album;

    public Cancion(String nombreCancion, int duracion) {
        this.nombreCancion = nombreCancion;
        this.duracion = duracion;
    }

    public Cancion() {

    }



    @Override
    public String toString() {
        return "Cancion{" +
                "id=" + id +
                ", nombreCancion='" + nombreCancion + '\'' +
                ", duracion=" + duracion;
    }



    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_cancion")
    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    @Basic
    @Column(name = "duracion")
    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cancion cancion = (Cancion) o;
        return id == cancion.id &&
                duracion == cancion.duracion &&
                Objects.equals(nombreCancion, cancion.nombreCancion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreCancion, duracion);
    }

    @ManyToOne
    @JoinColumn(name = "id_grupo", referencedColumnName = "id")
    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @ManyToOne
    @JoinColumn(name = "id_album", referencedColumnName = "id")
    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }
}
