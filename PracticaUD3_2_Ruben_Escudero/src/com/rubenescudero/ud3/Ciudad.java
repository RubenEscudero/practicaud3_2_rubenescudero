package com.rubenescudero.ud3;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Ciudad {
    private int id;
    private String nombreCiudad;
    private String pais;
    private List<Grupo> grupos;

    public Ciudad(String nombreCiudad, String pais) {
        this.nombreCiudad = nombreCiudad;
        this.pais = pais;
    }

    public Ciudad() {

    }

    @Override
    public String toString() {
        return "Ciudad " +
                "id=" + id +
                ", nombreCiudad='" + nombreCiudad + '\'' +
                ", pais='" + pais;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_ciudad")
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ciudad ciudad = (Ciudad) o;
        return id == ciudad.id &&
                Objects.equals(nombreCiudad, ciudad.nombreCiudad) &&
                Objects.equals(pais, ciudad.pais);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreCiudad, pais);
    }

    @OneToMany(mappedBy = "ciudad")
    public List<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<Grupo> grupos) {
        this.grupos = grupos;
    }
}
