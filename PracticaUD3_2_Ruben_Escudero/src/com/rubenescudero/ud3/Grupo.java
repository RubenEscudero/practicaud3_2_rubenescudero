package com.rubenescudero.ud3;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Grupo {
    private int id;
    private String nombreGrupo;
    private Date fechaCreacion;
    private List<Cancion> canciones;
    private Ciudad ciudad;

    public Grupo(String nombreGrupo, Date fechaCreacion) {
        this.nombreGrupo = nombreGrupo;
        this.fechaCreacion = fechaCreacion;
    }

    public Grupo() {

    }

    @Override
    public String toString() {
        return "Grupo" +
                "id=" + id +
                ", nombreGrupo='" + nombreGrupo + '\'' +
                ", fechaCreacion=" + fechaCreacion;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_grupo")
    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grupo grupo = (Grupo) o;
        return id == grupo.id &&
                Objects.equals(nombreGrupo, grupo.nombreGrupo) &&
                Objects.equals(fechaCreacion, grupo.fechaCreacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreGrupo, fechaCreacion);
    }

    @OneToMany(mappedBy = "grupo")
    public List<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<Cancion> canciones) {
        this.canciones = canciones;
    }

    @ManyToOne
    @JoinColumn(name = "id_ciudad", referencedColumnName = "id")
    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }
}
