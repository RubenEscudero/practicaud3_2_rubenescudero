package com.rubenescudero.ud3;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static Session session;

    public static void buildSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure();
        //Clases que hay que mapear
        configuration.addAnnotatedClass(Album.class);
        configuration.addAnnotatedClass(Artista.class);
        configuration.addAnnotatedClass(Cancion.class);
        configuration.addAnnotatedClass(Ciudad.class);
        configuration.addAnnotatedClass(Grupo.class);
        configuration.addAnnotatedClass(Instrumento.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    //Open new session
    public static void openSession() {
        session = sessionFactory.openSession();
    }

    //Returna actual session
    public static Session getCurrentSession(){
        if((session == null) || (!session.isOpen())){
            openSession();

        }
        return session;
    }

    //Close hibernate
    public static void closeSessionFactory() {
        if(session != null){
            session.close();
        }
        if(sessionFactory != null){
            sessionFactory.close();
        }
    }
}
