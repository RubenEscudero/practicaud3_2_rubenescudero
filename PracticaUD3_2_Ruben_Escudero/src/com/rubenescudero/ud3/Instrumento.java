package com.rubenescudero.ud3;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Instrumento {
    private int id;
    private String nombreInstrumento;
    private String material;
    private Artista artista;

    public Instrumento(String nombreInstrumento, String material) {
        this.nombreInstrumento = nombreInstrumento;
        this.material = material;
    }

    public Instrumento() {

    }

    @Override
    public String toString() {
        return "Instrumento{" +
                "id=" + id +
                ", nombreInstrumento='" + nombreInstrumento + '\'' +
                ", material='" + material + '\'' +
                ", artista=" + artista;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_instrumento")
    public String getNombreInstrumento() {
        return nombreInstrumento;
    }

    public void setNombreInstrumento(String nombreInstrumento) {
        this.nombreInstrumento = nombreInstrumento;
    }

    @Basic
    @Column(name = "material")
    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrumento that = (Instrumento) o;
        return id == that.id &&
                Objects.equals(nombreInstrumento, that.nombreInstrumento) &&
                Objects.equals(material, that.material);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreInstrumento, material);
    }

    @ManyToOne
    @JoinColumn(name = "id_artista", referencedColumnName = "id")
    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }
}
