package com.rubenescudero.ud3.gui;


import com.rubenescudero.ud3.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        init();
    }

    private void init() {
        addActionListeners(this);
        addListSelectionListeners(this);
    }

    private void addListSelectionListeners(Controlador controlador) {
        vista.listInstrumentos.addListSelectionListener(controlador);
        vista.listAlbumes.addListSelectionListener(controlador);
        vista.listArtistas.addListSelectionListener(controlador);
        vista.listCanciones.addListSelectionListener(controlador);
        vista.listGrupos.addListSelectionListener(controlador);
        vista.listCiudades.addListSelectionListener(controlador);
    }

    private void addActionListeners(ActionListener listener) {
        //panel instrumento
        vista.altaInstrumentoButton.addActionListener(listener);
        vista.borrarInstrumentoButton.addActionListener(listener);
        vista.modificarInstrumentoButton.addActionListener(listener);
        vista.listarInstrumentosButton.addActionListener(listener);
        //panel album
        vista.altaAlbumButton.addActionListener(listener);
        vista.eliminarAlbumButton.addActionListener(listener);
        vista.modificarAlbumButton.addActionListener(listener);
        vista.listarAlbumButton.addActionListener(listener);
        vista.anadirCancionAlubumbutton.addActionListener(listener);
        //panel artista
        vista.altaArtistaButton.addActionListener(listener);
        vista.eliminarArtistaButton.addActionListener(listener);
        vista.modifcarArtistaButton.addActionListener(listener);
        vista.listarAlbumButton.addActionListener(listener);
        //panel cancion
        vista.altaCancionButton.addActionListener(listener);
        vista.eliminarCancionButton.addActionListener(listener);
        vista.modficarCancionButton.addActionListener(listener);
        vista.listarCancionButton.addActionListener(listener);
        //panel grupo
        vista.instertarGrupoButton.addActionListener(listener);
        vista.eliminarGrupoButton.addActionListener(listener);
        vista.modificarGrupoButton.addActionListener(listener);
        vista.listarGrupoButton.addActionListener(listener);
        vista.ciudadGrupoButton.addActionListener(listener);
        //panel ciudad
        vista.insertarCiudadButton.addActionListener(listener);
        vista.eliminarCiudadButton.addActionListener(listener);
        vista.modificarCiudadButton.addActionListener(listener);
        vista.listarCiudadButton.addActionListener(listener);
        //menu
        vista.conectarItem.addActionListener(listener);
        vista.desconectarItem.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Conectar":
                modelo.conectar();
                listarInstrumentos(modelo.getInstrumentos());
                listarAlbumes(modelo.getAlbum());
                listarArtistas(modelo.getArtistas());
                listarCiudad(modelo.getCiudad());
                listarGrupos(modelo.getGrupo());
                listarCanciones(modelo.getCancion());
                break;
            case "Desconectar":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Alta instrumento":
                modelo.insertarInstrumento(new Instrumento(vista.txtNombreInstrumento.getText(), vista.txtMaterialInstrumento.getText()));
                listarInstrumentos(modelo.getInstrumentos());
                break;
            case "Borrar instrumento":
                Instrumento instrumentoEliminar = (Instrumento)vista.listInstrumentos.getSelectedValue();
                modelo.eliminarInstrumento(instrumentoEliminar);
                listarInstrumentos(modelo.getInstrumentos());
                break;
            case "Modificar instrumento":
                Instrumento instrumento = (Instrumento)vista.listInstrumentos.getSelectedValue();
                instrumento.setNombreInstrumento(vista.txtNombreInstrumento.getText());
                instrumento.setMaterial(vista.txtMaterialInstrumento.getText());
                modelo.modificarInstrumento(instrumento);
                listarInstrumentos(modelo.getInstrumentos());
                break;
            case "Listar instrumentos":
                listarInstrumentos(modelo.getInstrumentos());
                break;
            case "Alta album":
                modelo.insertarAlbum(new Album(vista.txtAlbum.getText(), Date.valueOf(vista.fechaLanzaminetoAlbum.getDate())));
                listarAlbumes(modelo.getAlbum());
                break;
            case "Eliminar album":
                modelo.eliminarAlbum((Album) vista.listAlbumes.getSelectedValue());
                listarAlbumes(modelo.getAlbum());
                break;
            case "Modificar album":
                Album album = (Album)vista.listAlbumes.getSelectedValue();
                album.setNombreAlbum(vista.txtAlbum.getText());
                album.setFechaLanzamiento(Date.valueOf(vista.fechaLanzaminetoAlbum.getDate()));
                modelo.modificarAlbum(album);
                listarAlbumes(modelo.getAlbum());
                break;
            case "Listar album":
                listarAlbumes(modelo.getAlbum());
                break;
            case "Alta artista":
                modelo.insertarArtista(new Artista(vista.txtNombreArtista.getText(), Date.valueOf(vista.fechaNacimientoArtista.getDate())));
                listarArtistas(modelo.getArtistas());
                break;
            case "Modificar artista":
                Artista artista = (Artista)vista.listArtistas.getSelectedValue();
                artista.setNombreArtista(vista.txtNombreArtista.getText());
                artista.setFechaNacimiento(Date.valueOf(vista.fechaNacimientoArtista.getDate()));
                modelo.modificarArtista(artista);
                listarArtistas(modelo.getArtistas());
                break;
            case "Eliminar artista":
                modelo.eliminarArtista((Artista)vista.listArtistas.getSelectedValue());
                listarArtistas(modelo.getArtistas());
                break;
            case "Listar artistas":
                listarArtistas(modelo.getArtistas());
                break;
                /*
            case "AlbumArtista":
                Artista artistaSeleccionado = (Artista)vista.listArtistas.getSelectedValue();
                artistaSeleccionado.getAlbumes().add((Album)vista.comboBoxAlbumesArtistas.getSelectedItem());
                modelo.modificarArtista(artistaSeleccionado);
                break;
                 */
            case "Alta cancion":
                modelo.insertarCancion(new Cancion(vista.txtNombreCancion.getText(), Integer.valueOf(vista.txtDuracionCancion.getText())));
                listarCanciones(modelo.getCancion());
                break;
            case "Modficar cancion":
                Cancion cancion = (Cancion)vista.listCanciones.getSelectedValue();
                cancion.setNombreCancion(vista.txtNombreCancion.getText());
                cancion.setDuracion(Integer.valueOf(vista.txtDuracionCancion.getText()));
                modelo.modificarCancion(cancion);
                listarCanciones(modelo.getCancion());
                break;
            case "Eliminar cancion":
                modelo.eliminarCancion((Cancion)vista.listCanciones.getSelectedValue());
                listarCanciones(modelo.getCancion());
                break;
            case "Listar cancion":
                listarCanciones(modelo.getCancion());
                break;
            case "Instertar grupo":
                modelo.insertarGrupo(new Grupo(vista.txtNombreGrupo.getText(), Date.valueOf(vista.fechaCreacionGrupo.getDate())));
                listarGrupos(modelo.getGrupo());
                break;
            case "Modificar grupo":
                Grupo grupo = (Grupo)vista.listGrupos.getSelectedValue();
                grupo.setNombreGrupo(vista.txtNombreGrupo.getText());
                grupo.setFechaCreacion(Date.valueOf(vista.fechaCreacionGrupo.getDate()));
                modelo.modificarGrupo(grupo);
                listarGrupos(modelo.getGrupo());
                break;
            case "Eliminar grupo":
                modelo.eliminarGrupo((Grupo)vista.listGrupos.getSelectedValue());
                listarGrupos(modelo.getGrupo());
                break;
            case "Listar grupo":
                listarGrupos(modelo.getGrupo());
                break;
            case "Insertar ciudad":
                modelo.insertarCiudad(new Ciudad(vista.txtNombreCiudad.getText(), vista.txtPais.getText()));
                listarCiudad(modelo.getCiudad());
                break;
            case "Modificar ciudad":
                Ciudad ciudad = (Ciudad)vista.listCiudades.getSelectedValue();
                ciudad.setNombreCiudad(vista.txtNombreCiudad.getText());
                ciudad.setPais(vista.txtPais.getText());
                modelo.modificarCiudad(ciudad);
                listarCiudad(modelo.getCiudad());
                break;
            case "Eliminar ciudad":
                modelo.eliminaCiudad((Ciudad)vista.listCiudades.getSelectedValue());
                listarCiudad(modelo.getCiudad());
                break;
            case "Listar ciudad":
                listarCiudad(modelo.getCiudad());
                break;
            case "Ciudad grupo":
                Grupo grupo1 = (Grupo)vista.listGrupos.getSelectedValue();
                grupo1.setCiudad((Ciudad)vista.cbCiudadesGrupo.getSelectedItem());
                modelo.modificarGrupo(grupo1);
                break;
            case "Cancion album":
                anadirCancionesAlbum();
                break;
        }
    }

    private void listarInstrumentos(List<Instrumento> list){
        vista.dlmInstrumentos.clear();
        for(Instrumento instrumento: list){
            vista.dlmInstrumentos.addElement(instrumento);
        }
    }

    private void listarAlbumes(List<Album> list){
        vista.dlmAlbumes.clear();
        for(Album album: list){
            vista.dlmAlbumes.addElement(album);
        }
    }

    private void listarArtistas(List<Artista> list){
        vista.dlmArtistas.clear();
        for(Artista artista: list){
            vista.dlmArtistas.addElement(artista);

        }
    }

    private void listarCanciones(List<Cancion> list){
        vista.dlmCancion.clear();
        for(Cancion cancion: list){
            vista.dlmCancion.addElement(cancion);
        }
    }

    private void listarGrupos(List<Grupo> list){
        vista.dlmGrupo.clear();
        for(Grupo grupo: list){
            vista.dlmGrupo.addElement(grupo);
        }
    }

    private void listarCiudad(List<Ciudad> list){
        vista.dlmCiudad.clear();
        for(Ciudad ciudad: list){
            vista.dlmCiudad.addElement(ciudad);
            vista.cbCiudadesGrupo.addItem(ciudad);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listInstrumentos){
                Instrumento instrumento = (Instrumento)vista.listInstrumentos.getSelectedValue();
                vista.txtNombreInstrumento.setText(instrumento.getNombreInstrumento());
                vista.txtMaterialInstrumento.setText(instrumento.getMaterial());
            }
            if(e.getSource() == vista.listArtistas){
                Artista artista = (Artista)vista.listArtistas.getSelectedValue();
                vista.txtNombreArtista.setText(artista.getNombreArtista());
                vista.fechaNacimientoArtista.setDate(artista.getFechaNacimiento().toLocalDate());
            }
            if(e.getSource() == vista.listAlbumes){
                Album album = (Album)vista.listAlbumes.getSelectedValue();
                vista.txtAlbum.setText(album.getNombreAlbum());
                vista.fechaLanzaminetoAlbum.setDate(album.getFechaLanzamiento().toLocalDate());
                listarCancionesAlbum(album);
            }
            if(e.getSource() == vista.listCiudades){
                Ciudad ciudad = (Ciudad)vista.listCiudades.getSelectedValue();
                vista.txtNombreCiudad.setText(ciudad.getNombreCiudad());
                vista.txtPais.setText(ciudad.getPais());
            }
            if(e.getSource() == vista.listCanciones){
                Cancion cancion = (Cancion)vista.listCanciones.getSelectedValue();
                vista.txtNombreCancion.setText(cancion.getNombreCancion());
                vista.txtDuracionCancion.setText(String.valueOf(cancion.getDuracion()));
            }
            if(e.getSource() == vista.listGrupos){
                Grupo grupo = (Grupo)vista.listGrupos.getSelectedValue();
                vista.txtNombreGrupo.setText(grupo.getNombreGrupo());
                vista.fechaCreacionGrupo.setDate(grupo.getFechaCreacion().toLocalDate());
                try {
                    Ciudad ciudad = modelo.getCiudadGrupo(grupo.getCiudad().getId());
                    vista.lblCiudadGrupo.setText(ciudad.toString());
                }catch (Exception en){
                    en.printStackTrace();
                    vista.lblCiudadGrupo.setText("");
                }

            }
        }
    }

    private void anadirCancionesAlbum(){
        Album album = (Album)vista.listAlbumes.getSelectedValue();
        SeleccionarCancionesDialog dialog = new SeleccionarCancionesDialog();
        dialog.mostrarDialogo();
        album.getCanciones().addAll(dialog.getCancionesSeleccionadas());
        modelo.insertarCancionesAlbum(album);
        listarCancionesAlbum(album);
    }

    private void listarCancionesAlbum(Album album){
        List<Cancion> listCanciones = album.getCanciones();
        vista.dlmCancionAlbum.clear();
        for(Cancion cancion: listCanciones){
            vista.dlmCancionAlbum.addElement(cancion);
        }
    }
}
