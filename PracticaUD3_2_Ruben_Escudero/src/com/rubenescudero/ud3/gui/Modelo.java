package com.rubenescudero.ud3.gui;

import com.rubenescudero.ud3.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import sun.plugin2.os.windows.SECURITY_ATTRIBUTES;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class Modelo {
    SessionFactory sessionFactory;

    public void desconectar() {
        HibernateUtil.closeSessionFactory();
        /*
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();

         */
    }

    public void conectar() {
        HibernateUtil.buildSessionFactory();
        /*
        Configuration configuration = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuration.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuration.addAnnotatedClass(Album.class);
        configuration.addAnnotatedClass(Artista.class);
        configuration.addAnnotatedClass(Cancion.class);
        configuration.addAnnotatedClass(Ciudad.class);
        configuration.addAnnotatedClass(Grupo.class);
        configuration.addAnnotatedClass(Instrumento.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuration.buildSessionFactory(ssr);
        */
    }

    public void insertarInstrumento(Instrumento instrumento){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(instrumento);
        session.getTransaction().commit();
        session.close();
    }

    public void eliminarInstrumento(Instrumento instrumento){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(instrumento);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarInstrumento(Instrumento instrumento) {
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(instrumento);
        session.getTransaction().commit();
        session.close();
    }

    public ArrayList<Instrumento> getInstrumentos(){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Instrumento");
        ArrayList<Instrumento> lista = (ArrayList<Instrumento>)query.getResultList();
        session.close();
        return lista;
    }

    public void insertarAlbum(Album album){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(album);
        session.getTransaction().commit();
        session.close();
    }

    public void eliminarAlbum(Album album){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(album);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarAlbum(Album album){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(album);
        session.getTransaction().commit();
        session.close();
    }

    public List<Album> getAlbum(){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Album ");
        List<Album> list = (List<Album>)query.getResultList();
        session.close();
        return list;
    }

    public void insertarArtista(Artista artista){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(artista);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarArtista(Artista artista){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(artista);
        session.getTransaction().commit();
        session.close();
    }

    public void eliminarArtista(Artista artista){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(artista);
        session.getTransaction().commit();
        session.close();
    }

    public List<Artista> getArtistas(){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Artista ");
        List<Artista> list = (List<Artista>)query.getResultList();
        session.close();
        return list;
    }


    public void insertarCancion(Cancion cancion){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(cancion);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarCancion(Cancion cancion){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(cancion);
        session.getTransaction().commit();
        session.close();
    }

    public void eliminarCancion(Cancion cancion){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(cancion);
        session.getTransaction().commit();
        session.close();
    }

    public List<Cancion> getCancion(){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Cancion ");
        List<Cancion> list = (List<Cancion>)query.getResultList();
        session.close();
        return list;
    }

    public void insertarGrupo(Grupo grupo){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(grupo);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarGrupo(Grupo grupo){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(grupo);
        session.getTransaction().commit();
        session.close();

    }

    public void eliminarGrupo(Grupo grupo){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(grupo);
        session.getTransaction().commit();
        session.close();

    }

    public List<Grupo> getGrupo(){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Grupo ");
        List<Grupo> list = (List<Grupo>)query.getResultList();
        session.close();
        return list;
    }

    public void insertarCiudad(Ciudad ciudad){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(ciudad);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarCiudad(Ciudad ciudad){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(ciudad);
        session.getTransaction().commit();
        session.close();

    }

    public void eliminaCiudad(Ciudad ciudad){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(ciudad);
        session.getTransaction().commit();
        session.close();

    }

    public List<Ciudad> getCiudad(){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Ciudad ");
        List<Ciudad> list = (List<Ciudad>)query.getResultList();
        session.close();
        return list;
    }

    public Ciudad getCiudadGrupo(int idCiudad){
        Session session = HibernateUtil.getCurrentSession();
        Query query = session.createQuery("FROM Ciudad WHERE id = :id");
        query.setParameter("id", idCiudad);
        Ciudad ciudad = (Ciudad)query.getSingleResult();
        session.close();
        return ciudad;
    }

    public void insertarCancionesAlbum(Album album){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        for(Cancion cancion : album.getCanciones()){
            cancion.setAlbum(album);
        }
        session.getTransaction().commit();
        session.close();
    }
}
