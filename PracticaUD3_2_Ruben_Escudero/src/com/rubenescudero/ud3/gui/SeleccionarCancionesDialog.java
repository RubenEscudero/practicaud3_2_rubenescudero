package com.rubenescudero.ud3.gui;

import com.rubenescudero.ud3.Cancion;
import com.rubenescudero.ud3.HibernateUtil;

import javax.persistence.Query;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class SeleccionarCancionesDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList<Cancion> list1;
    private DefaultListModel<Cancion> dlmCancion;
    private List<Cancion> cancionesSeleccionadas;

    public List<Cancion> getCancionesSeleccionadas() {
        return cancionesSeleccionadas;
    }

    public void setCancionesSeleccionadas(List<Cancion> cancionesSeleccionadas) {
        this.cancionesSeleccionadas = cancionesSeleccionadas;
    }

    public SeleccionarCancionesDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        cancionesSeleccionadas = list1.getSelectedValuesList();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void mostrarDialogo() {
        listarCanciones();
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void listarCanciones() {
        dlmCancion = new DefaultListModel<>();
        list1.setModel(dlmCancion);

        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cancion ");
        List<Cancion> listaCanciones = query.getResultList();

        for(Cancion cancion : listaCanciones){
            dlmCancion.addElement(cancion);
        }
    }
}
