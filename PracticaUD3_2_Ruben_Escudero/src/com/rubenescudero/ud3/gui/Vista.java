package com.rubenescudero.ud3.gui;



import com.github.lgooddatepicker.components.DatePicker;
import com.rubenescudero.ud3.*;

import javax.swing.*;
import java.awt.*;

public class Vista {
    private JFrame frame;
    JTabbedPane tabbedPane1;
    JPanel panel1;
    JTextField txtNombreInstrumento;
    JTextField txtMaterialInstrumento;
    JButton altaInstrumentoButton;
    JButton modificarInstrumentoButton;
    JButton borrarInstrumentoButton;
    JList listInstrumentos;
    JButton listarInstrumentosButton;
    JComboBox comboBoxAlbumesArtistas;
    JComboBox comboBoxInstumentosArtista;
    JButton anadirInstrumentoArtistaButton;
    JButton anadirAlbumArtistaButton;
    JButton altaArtistaButton;
    JButton modifcarArtistaButton;
    JButton eliminarArtistaButton;
    JButton listarArtistasButton;
    JList listArtistas;
    JTextField txtNombreArtista;
    JTextField txtAlbum;
    JButton altaAlbumButton;
    JButton modificarAlbumButton;
    JButton eliminarAlbumButton;
    JButton listarAlbumButton;
    JList listAlbumes;
    DatePicker fechaLanzaminetoAlbum;
    DatePicker fechaNacimientoArtista;
    JList listInstrumentosDeArtista;
    JList listAlbumesDeArtistas;
    JTextField txtNombreCancion;
    JTextField txtDuracionCancion;
    JButton altaCancionButton;
    JButton modficarCancionButton;
    JButton eliminarCancionButton;
    JButton listarCancionButton;
    JList listCanciones;
    JTextField txtNombreGrupo;
    DatePicker fechaCreacionGrupo;
    JButton instertarGrupoButton;
    JButton modificarGrupoButton;
    JButton eliminarGrupoButton;
    JButton listarGrupoButton;
    JList listGrupos;
    JTextField txtNombreCiudad;
    JTextField txtPais;
    JButton insertarCiudadButton;
    JButton modificarCiudadButton;
    JButton eliminarCiudadButton;
    JButton listarCiudadButton;
    JList listCiudades;
    JButton ciudadGrupoButton;
    JComboBox cbCiudadesGrupo;
    JLabel lblCiudadGrupo;
    JButton anadirCancionAlubumbutton;
    JComboBox cbCancionAlbum;
    JList listCancionesAlbum;

    JMenuItem conectarItem;
    JMenuItem desconectarItem;

    DefaultListModel<Instrumento> dlmInstrumentos;
    DefaultListModel<Album> dlmAlbumes;
    DefaultListModel<Artista> dlmArtistas;
    DefaultListModel<Album> dlmAlbumArtista;
    DefaultListModel<Cancion> dlmCancion;
    DefaultListModel<Grupo> dlmGrupo;
    DefaultListModel<Ciudad> dlmCiudad;
    DefaultListModel<Cancion> dlmCancionAlbum;

    DefaultComboBoxModel<Ciudad> dcmCiudad;
    DefaultComboBoxModel<Cancion> dcmCancion;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(820, 580));
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        createMenu();
        addDefaultListModels();
        addDeafultComboBoxModels();
    }

    private void addDeafultComboBoxModels() {
        dcmCiudad = new DefaultComboBoxModel<>();
        cbCiudadesGrupo.setModel(dcmCiudad);
    }

    //Inicializate table model
    private void addDefaultListModels() {
        dlmInstrumentos = new DefaultListModel<>();
        listInstrumentos.setModel(dlmInstrumentos);
        dlmAlbumes = new DefaultListModel<>();
        listAlbumes.setModel(dlmAlbumes);
        dlmArtistas = new DefaultListModel<>();
        listArtistas.setModel(dlmArtistas);
        dlmCancion = new DefaultListModel<>();
        listCanciones.setModel(dlmCancion);
        dlmGrupo = new DefaultListModel<>();
        listGrupos.setModel(dlmGrupo);
        dlmCiudad = new DefaultListModel<>();
        listCiudades.setModel(dlmCiudad);
        dlmCancionAlbum = new DefaultListModel<>();
        listCancionesAlbum.setModel(dlmCancionAlbum);
    }

    //Add menu to frame
    private void createMenu() {
        JMenu menu = new JMenu("Opciones");

        conectarItem = new JMenuItem("Conectar");
        conectarItem.setActionCommand("Conectar");
        menu.add(conectarItem);
        desconectarItem = new JMenuItem("Desconectar");
        desconectarItem.setActionCommand("Desconectar");
        menu.add(desconectarItem);

        JMenuBar jMenuBar = new JMenuBar();
        jMenuBar.add(menu);
        frame.setJMenuBar(jMenuBar);
    }

}
